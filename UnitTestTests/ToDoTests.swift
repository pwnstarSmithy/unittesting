//
//  ToDoTests.swift
//  UnitTestTests
//
//  Created by pwnstarSmithy on 13/10/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import XCTest

@testable import UnitTest

class ToDoTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    
    func test_Init_WhenGivenTitle_SetsTitle(){
        
       let item = ToDoItem(title: "Foo")
        XCTAssertEqual(item.title, "Foo", "Should set title")
        
    }
    
    func test_Init_WhenGivenDescriptionSets_SetsDescription(){
        
        let item = ToDoItem(title: "", itemDescription: "Get a mop and broom" )
        
        XCTAssertEqual(item.itemDescription, "Get a mop and broom", "Should set itemDescription")
    }

    func test_Init_SetsTimestamp(){
        
        let item = ToDoItem(title: "", timeStamp: 0.0)
        
        XCTAssertEqual(item.timeStamp,0.0, "should set timestamp")
        
    }
    
    func test_Init_WhenGivenLocation_SetsLocation(){
        
        
        let location = Location(name: "Home")
        
        
    }
    
    func test_Init_SetsLocation() {
        
        let location = Location(name: "Foo")
        let item = ToDoItem(title: "",
                            location: location)
        
        XCTAssertEqual(item.location?.name,
                       location.name,
                       "should set location")
    } 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
