//
//  ViewController.swift
//  UnitTest
//
//  Created by pwnstarSmithy on 11/10/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    func numberOfVowelsInString(string: String) -> Int{
        
        let vowels : [Character] = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"]
        
        var numberofVowels = 0
        
        for character in string{
            
            if vowels.contains(character){
                
                numberofVowels += 1
                
            }
            
        }
        return numberofVowels
    }
    
    func makeHeadline(from string: String) -> String{
        
        let words =  string.components(separatedBy: " ")
        
        let headlineWords = words.map { (word) -> String in
            
            var mutableWord = word
            
            let first = mutableWord.remove(at: mutableWord.startIndex)
            
            return String(first).uppercased() + mutableWord
            
        }
        
        return headlineWords.joined(separator: " ")
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

