//
//  ToDoItem.swift
//  UnitTest
//
//  Created by pwnstarSmithy on 13/10/2018.
//  Copyright © 2018 pwnstarSmithy. All rights reserved.
//

import Foundation

struct ToDoItem {
    
    let title: String
   
    let itemDescription : String?
    
    let timeStamp : Double?
    
    let location : Location?
    
    init(title: String,
         itemDescription: String? = nil,
         timeStamp: Double? = nil,
         location: Location? = nil) {
        
        
        self.title = title
        self.itemDescription = itemDescription
        self.timeStamp = timeStamp
        self.location = location
    }
}
